class Board
  attr_reader :grid

  def initialize(grid = nil)
    @grid = grid if !grid.nil?
    @grid = Array.new(3) { Array.new(3) } if grid.nil?
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    # Win logic here. Return a mark. Return nil if there is no winner.
    (0..2).each do |i|
      # Detect row wins
      return grid[i][0] if grid[i].uniq.length == 1 && !grid[i][0].nil?

      # Detect column wins (kinda clumsy...)
      column = []
      (0..2).each do |j|
        column << grid[j][i]
      end
      return column[0] if column.uniq.length == 1 && !column.uniq[0].nil?
    end

    # Detect diagonal wins (clumsy as well...)
    diagonal1 = [grid[0][0], grid[1][1], grid[2][2]]
    diagonal2 = [grid[0][2], grid[1][1], grid[2][0]]

    same1 = diagonal1.uniq.length == 1 && !diagonal1.uniq.nil?
    same2 = diagonal2.uniq.length == 1 && !diagonal2.uniq.nil?
    return grid[1][1] if same1 || same2
  end

  def over?
    # Game is over if there is a win, or if the board is full
    winner != nil || grid.all? { |row| !row.include?(nil) }
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    grid[row][col] = mark
  end
end
