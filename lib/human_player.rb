class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = :X
  end

  def display(board)
    board.grid.each { |row| puts "#{row}\n" }
  end

  def get_move
    puts "Where do you want to move? (e.g. 0,0) "
    pos = gets.chomp.split(" ").map(&:to_i)
  end
end
