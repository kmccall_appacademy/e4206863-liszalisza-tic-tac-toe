class ComputerPlayer
  attr_reader :board
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = :O
  end

  def display(board)
    @board = board
  end

  def get_move
    # Return winning move if available, else place_mark in random pos.
    # Get all empty positions and try placing a mark on them and see if winner == :O ?
    empty_pos = []

    board.grid.each_index do |row|
      board.grid[row].each_index do |col|
        pos = [row, col]

        if board.empty?(pos)
          empty_pos << pos # push all empty positions into array
          board.place_mark(pos, @mark)
          if board.winner == @mark
            return pos
          end
          # overwrite mark with nil if it didn't lead to a win. Seems cumbersome but works...
          board.place_mark(pos, nil)
        end
      end
    end
    # Make random move in case above returns nothing. Return random pos
    random_pos = empty_pos.shuffle.first
  end
end
